<div class="container">
<div class="row">
<footer class="footer">
    <span class="text-muted col-12 col-sm-6 col-md-8">Copyright &copy; 2017 by <a href="mailto:rafinyadi@gmail.com">Rafi</a>.  All Rights Reserved.</span>
    <span class="text-muted col-6 col-md-4"><a class="r-footext" href="/sk">Kebijakan Privasi</a>  <a class="r-footext" href="/tentang">Syarat Dan Ketentuan</a><a class="r-footext" href="/tentang">Tentang</a></span>
</footer>
