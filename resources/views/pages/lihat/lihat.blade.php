@extends('layouts.app')
@section('header')
        <a href="/">Cari Kendaraan</a>
@endsection
@section('css')
    @include('pages.lihat.css')
@endsection

@section('content')
 <div class="panel panel-primary">
              <div class="panel-heading" style="background-color: #9d6617;">Informasi Kendaraan Hilang # </div>
              <table class="table table-bordered" style="color: #9d6617;">
               
                <tbody>
                  <tr class="danger">
                    <td>
                    @foreach($show as $show)
                    Nama: {{ $show['nama'] }}<br>
                    Email: {{ $show['email'] }}<br>
                    Plat Nomer: {{ $show['platno'] }}<br>
                    Kehilangan di Kota: {{ $show['kota'] }}<br>
                    Jenis Kendaraan: {{ $show['jeniskendaraan']}}<br>
                    Peristiwa: {{ $show['peristiwa'] }}<br>
                    Keterangan Tambahan {{ $show['keterangantambahan'] }}
                    @break;
                    @endforeach
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
    </div>
</div>

@endsection
@section('js')
    @include('pages.lihat.js')
@endsection