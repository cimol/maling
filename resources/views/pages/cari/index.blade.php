@extends('layouts.app')
@section('header')
        <a href="/">Cari Kendaraan</a>
@endsection
@section('css')
    @include('pages.lihat.css')
@endsection

@section('content')
 <div class="panel panel-primary">
              <div class="panel-heading" style="background-color: #9d6617;">Informasi Kendaraan Hilang # Hasil Pencarian</div>
              <table class="table table-bordered" style="color: #9d6617;">
               <thead>
               <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Plat Nomer</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($show as $data)
                  <tr class="danger">
                    <td><a href="lihat/{{ $data['id'] }}">{{ $data['id'] }}</a></td>
                    <td>{{ $data['nama'] }}</td>
                    <td>{{ $data['platno'] }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
    </div>
</div>

@endsection
@section('js')
    @include('pages.lihat.js')
@endsection