<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','API\User\laporController@homeLapor');
Route::get('/lapor','API\User\laporController@frontLapor');
Route::post('/lapor/submit','API\User\laporController@addLapor');

Route::get('/lihat/{id}','API\User\laporController@showIdLapor');
Route::get('/semua','API\User\laporController@showLapor');
Route::post('/cari','API\User\laporController@searchLapor');